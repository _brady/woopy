from time import sleep
from woopy.sdk import Analog
from woopy.wooting_types import DeviceEventType, DeviceInfo, AnalogResult

ANALOG_WRAPPER_PATH = 'wooting_analog_wrapper.dll'  # update this to your path
run = True


def test_cb(event_type: DeviceEventType, device_info: DeviceInfo):
    global run
    if event_type == DeviceEventType.disconnected:
        run = False
        print(f'Disconnect of {device_info.device_name.decode()} detected.')
    elif event_type == DeviceEventType.connected:
        run = True
        print(f'Connect of {device_info.device_name.decode()} detected.')


kbd = Analog(ANALOG_WRAPPER_PATH)
init_result = kbd.initialize()
if init_result is AnalogResult.no_devices:
    print('No Wooting found. :(')
elif init_result is AnalogResult.ok:
    print('Wooting connected.')
    kbd.set_device_event_cb(test_cb)

    print('Test analog feature by pressing any keys. Press ESC to continue.')
    while not kbd.read_analog(41):
        pressed_keys = kbd.read_full_buffer()
        if pressed_keys:
            for key, value in zip(*pressed_keys):
                print(f'{key}: {value}')
        sleep(0.1)

    print('Test the disconnect callback by disconnecting you keyboard now.')
    while run:
        sleep(0.1)
    print('Waiting for reconnect.')
    while not run:
        sleep(0.1)
    print('Keyboard reconnected.')

else:
    print('Could not initialize SDK. :(')
print('Demo finished.')
