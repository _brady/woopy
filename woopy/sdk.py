import ctypes as c
from typing import List, Callable, Union
from .wooting_types import DeviceEventType, KeycodeType, AnalogResult, DeviceInfo


class Analog:
    """
    The Wooting keyboard class is a wrapper for the Wooting Analog SKD for
    use in Python.

    It is best used with the public methods of the Wooting class for easy
    use and documentation, but all SDK functions can also be called by
    their original names, e.g.

    kbd = Wooting(...)
    kbd.initialize()
    kbd.read_analog(code)

    """

    __analog_methods = ['wooting_analog_initialise',
                        'wooting_analog_is_initialised',
                        'wooting_analog_uninitialise',
                        'wooting_analog_set_keycode_mode',
                        'wooting_analog_read_analog',
                        'wooting_analog_read_analog_device',
                        'wooting_analog_set_device_event_cb',
                        'wooting_analog_clear_device_event_cb',
                        'wooting_analog_get_connected_devices_info',
                        'wooting_analog_read_full_buffer',
                        'wooting_analog_read_full_buffer_device']

    def __init__(self, path: str = None):
        """ Initialize Wooting class by loading Wooting Analog and/or RGB SDK DLLs. """

        self.__analog = None
        if path is not None:
            self.__analog = c.CDLL(path)

    def __getattr__(self, item, *args, **kwargs):
        def _return(*args, **kwargs):
            return False

        if item in self.__analog_methods:
            if self.__analog:
                return getattr(self.__analog, item, *args, **kwargs)
            print('Wooting analog SDK not loaded.')
            return _return
        raise AttributeError('Attribute or method does not exist, use the public methods or refer '
                             'to the official Wooting SDK documentation for a list of usable methods.')

    def __enter__(self):
        self.initialize()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.__analog:
            self.uninitialize()

    def __del__(self):
        if self.__analog:
            self.uninitialize()

    def initialize(self) -> AnalogResult:
        """
        Initializes the Analog SDK, this needs to be successfully called
        before any other functions of the SDK can be called.

        :return: AnalogResult with ok, no_devices, no_plugins or
                 function_not_found.

        Expected Returns:
         - ok: Meaning the SDK initialised successfully (currently also means
           that there is at least one plugin initialized with at least one
           device connected)
         - no_devices: Meaning the SDK initialised successfully, but no devices
           are connected
         - no_plugins: Meaning that either no plugins were found or some were
           found but none were successfully initialized
         - function_not_found: Indicates that the SDK was not found, either it
           is not installed or it hasn't been added to the PATH
        """

        self.wooting_analog_initialise.restype = AnalogResult
        return self.wooting_analog_initialise()

    def is_initialized(self) -> bool:
        """
        Returns a bool indicating if the Analog SDK has been initialized.

        :return: Bool indicating if the Analog SDK has been initialized.
        """
        return bool(self.wooting_analog_is_initialised())

    def uninitialize(self) -> AnalogResult:
        """
        Uninitializes the SDK, returning it to an empty state, similar to how
        it would be before first initialisation.

        :return: AnalogResult with ok.

        Expected Returns:
         - ok: Indicates that the SDK was successfully uninitialised
        """

        return AnalogResult(self.wooting_analog_uninitialise())

    def set_keycode_mode(self, keycode_type: KeycodeType) -> AnalogResult:
        """
        Sets the type of Keycodes the Analog SDK will receive (in read_analog)
        and output (in read_full_buffer).

        :param keycode_type: The keycode mode to use as a KeycodeType.
        :return: AnalogResult with ok, invalid_argument, not_available or
                 uninitialized.

        Expected Returns.
         - ok: The Keycode mode was changed successfully
         - invalid_argument: The given KeycodeType is not one supported by
           the SDK
         - not_available: The given KeycodeType is present, but not supported
           on the current platform
         - uninitialized: The SDK is not initialized

        Notes:
         - By default, the mode is set to HID
         - VirtualKey and VirtualKeyTranslate are only available on Windows
         - With all modes except VirtualKeyTranslate, the key identifier will
           point to the physical key on the standard layout. i.e. if you ask
           for the Q key, it will be the key right to tab regardless of the
           layout you have selected
         - With VirtualKeyTranslate, if you request Q, it will be the key that
           inputs Q on the current layout, not the key that is Q on the
           standard layout.
        """

        return AnalogResult(self.wooting_analog_set_keycode_mode(keycode_type.value))

    def read_analog(self, code: int) -> Union[float, AnalogResult]:
        """
        Reads the Analog value of the key with identifier code from any
        connected device. The set of key identifiers that is used depends on
        the Keycode mode set using analog_set_mode.

        :param code: USB HID code of a key.
        :return: Analog value of the key with given id code if successful,
                 otherwise AnalogResult with error.

        Note:
         See following url for USB HID codes:
         https://www.win.tue.nl/~aeb/linux/kbd/scancodes-10.html#scancodesets

        Expected Returns:
         - 0.0 - 1.0: The Analog value of the key with the given id code
         - AnalogResult.no_mapping: No keycode mapping was found from the
           selected mode (set by analog_set_mode) and HID.
         - AnalogResult.uninitialized: The SDK is not initialised
         - AnalogResult.no_devices: There are no connected devices
        """

        self.wooting_analog_read_analog.restype = c.c_float
        result = self.wooting_analog_read_analog(code)
        if result < 0:
            return AnalogResult(int(result))
        return result

    def read_analog_device(self, code: int, device_id: int) -> Union[float, AnalogResult]:
        """
        Reads the Analog value of the key with identifier code from the device
        with id device_id. The set of key identifiers that is used depends on
        the Keycode mode set using analog_set_mode.

        :param code: USB HID code of a key.
        :param device_id: Unique device id.
        :return: Analog value of the key with given id code if successful,
                 otherwise AnalogResult with error.

        Note:
         - See following url for USB HID codes:
           https://www.win.tue.nl/~aeb/linux/kbd/scancodes-10.html#scancodesets
         - The device_id can be found by calling get_connected_devices_info
           and getting the device_id from one of the DeviceInfo classes

        Expected Returns:
         - 0.0 - 1.0: The Analog value of the key with the given id code
         - AnalogResult.no_mapping: No keycode mapping was found from the
           selected mode (set by analog_set_mode) and HID.
         - AnalogResult.uninitialized: The SDK is not initialised
         - AnalogResult.no_devices: There are no connected devices with
           id device_id
        """

        self.wooting_analog_read_analog_device.argtypes = [c.c_short, c.c_uint64]
        self.wooting_analog_read_analog_device.restype = c.c_float
        result = self.wooting_analog_read_analog(code, device_id)
        if result < 0:
            return AnalogResult(int(result))
        return result

    def set_device_event_cb(self, fn: Callable[[DeviceEventType, DeviceInfo], None]) -> AnalogResult:
        """
        Set the callback which is called when there is a DeviceEvent.
        Currently these events can either be Disconnected or Connected
        (Currently not properly implemented).

        :param fn: A callback python function accepting DeviceEventType
                   and DeviceInfo.
        :return: AnalogResult with ok or uninitialized.

        Note:
         The callback gets given the type of event DeviceEventType and the
         DeviceInfo class that the event applies to. The function signature
         should look similar to this:
         fn(event_type: DeviceEventType, device_info: DeviceInfo)

        Expected Returns:
         - ok: The callback was set successfully
         - uninitialized: The SDK is not initialized
        """

        def cb(event_type, device_info_pointer):
            event_type = DeviceEventType(event_type)
            fn(event_type, device_info_pointer.contents)

        self.__event_cb = c.CFUNCTYPE(c.c_void_p, c.c_void_p, c.POINTER(DeviceInfo))(cb)
        if self.__analog:
            return AnalogResult(self.wooting_analog_set_device_event_cb(self.__event_cb))

    def clear_device_event_cb(self) -> AnalogResult:
        """
        Clears the device event callback that has been set.

        :return: AnalogResult with ok or uninitialized

        Expected Returns:
         - ok: The callback was cleared successfully
         - uninitialized: The SDK is not initialised
        """

        result = AnalogResult(self.wooting_analog_clear_device_event_cb())
        try:
            del self.__event_cb
        except AttributeError:
            pass
        return result

    def get_connected_devices_info(self, length: int = 10) -> Union[AnalogResult, List[DeviceInfo]]:
        """
        Returns a list of DeviceInfo objects for each device found,
        up to 'length' devices.

        :param length: Maxiumum number of DeviceInfo to return.
        :return: List of DeviceInfo objects for devices found or AnalogResult
                 if an error occurred.

        Expected Returns:
         - uninitialized: Indicates that the AnalogSDK hasn't been initialized
        """

        buffer = (c.POINTER(DeviceInfo) * length)()
        result = int(self.wooting_analog_get_connected_devices_info(buffer, length))
        if result < 0:
            return AnalogResult(result)
        return [pointer.contents for pointer in buffer[:result]]

    def read_full_buffer(self, length: int = 100):
        """
        Reads all the analog values for pressed keys for all devices and
        combines their values, filling up code_buffer with the keycode
        identifying the pressed key and fills up analog_buffer with the
        corresponding float analog values. i.e. The analog value for they key
        at index 0 of code_buffer, is at index 0 of analog_buffer.

        :param length: Maxiumum number of keys to read.
        :return: Either a tuple of two list the first with key codes of
                 pressed keys, the second with the keys analog values or
                 AnalogResult if an error occured.

        Notes:
         - The codes that are filled into the code_buffer are of the
           KeycodeType set with analog_set_mode.
         - If two devices have the same key pressed, the greater value will be given.

        Expected Returns
         - uninitialized: Indicates that the AnalogSDK hasn't been initialised
         - no_devices: Indicates no devices are connected
        """

        code_buffer = (c.c_short * length)()
        analog_buffer = (c.c_float * length)()
        result = int(self.wooting_analog_read_full_buffer(code_buffer, analog_buffer, length))
        if result < 0:
            return AnalogResult(result)
        return code_buffer[:result], analog_buffer[:result]

    def read_full_buffer_device(self, device_id, length: int = 100):
        """
        Reads all the analog values for pressed keys for the device with id
        device_id, filling up code_buffer with the keycode identifying the
        pressed key and fills up analog_buffer with the corresponding float
        analog values. i.e. The analog value for they key at index 0 of
        code_buffer, is at index 0 of analog_buffer.

        :param device_id: Unique device id.
        :param length: Maxiumum number of keys to read.
        :return: Either a tuple of two list the first with key codes of
                 pressed keys, the second with the keys analog values or
                 AnalogResult if an error occured.

        Notes:
         - The codes that are filled into the code_buffer are of the
           KeycodeType set with analog_set_mode.

        Expected Returns
         - uninitialized: Indicates that the AnalogSDK hasn't been initialized
         - no_devices: Indicates the device with id device_id is not connected
        """

        code_buffer = (c.c_short * length)()
        analog_buffer = (c.c_float * length)()
        device_id = c.c_uint64(device_id)
        result = int(self.wooting_analog_read_full_buffer_device(code_buffer, analog_buffer, length, device_id))
        if result < 0:
            return AnalogResult(result)
        return code_buffer[:result], analog_buffer[:result]
