import ctypes as c
from dataclasses import dataclass
from enum import IntEnum


class CtypesEnum(IntEnum):
    """
    Ctypes IntEnum superclass
    """

    @classmethod
    def from_param(cls, obj):
        return int(obj)


class DeviceEventType(CtypesEnum):
    """
    Wooting device event type.

    :cvar connected: Device has been connected
    :cvar disconnected: Device has been disconnected
    """

    connected = 1
    disconnected = 2


class KeycodeType(CtypesEnum):
    """
    By default the SDK will use the USB HID codes (see table 10.6) to identify
    keys. This can be changed using the wooting_analog_set_keycode_mode
    function, which changes the keycodes taken by read_analog and the
    keycodes given in read_full_buffer. The available options are:

     - HID: The standard USB HID codes (default) List on table 10.6
       https://www.win.tue.nl/~aeb/linux/kbd/scancodes-10.html#scancodesets
     - ScanCode1: Scan codes set 1, see Set 1 column on table 10.6
       https://www.win.tue.nl/~aeb/linux/kbd/scancodes-10.html#scancodesets
       (Escape codes can be given as either a 0x1 or 0xE0 prefix)
     - VirtualKey: Windows Virtual Key codes
       https://docs.microsoft.com/en-gb/windows/win32/inputdev/virtual-key-codes
     - VirtualKeyTranslate: Windows Virtual Key codes but they are translated
       based on layout, so requesting the letter Q gets the key that inputs Q
       on the selected layout, rather than always getting the key right of
       tab (the standard Q position) like VirtualKey would.
       https://docs.microsoft.com/en-gb/windows/win32/inputdev/virtual-key-codes

    :cvar hid: USB HID Keycodes https://www.usb.org/document-library/hid-usage-tables-112 pg53
    :cvar scancode1: Scan code set 1
    :cvar virtual_key: Windows Virtual Keys
    :cvar virtual_key_translate: Windows Virtual Keys which are translated to the current keyboard locale
    """

    hid = 0
    scancode1 = 1
    virtual_key = 2
    virtual_key_translate = 3


class AnalogResult(CtypesEnum):
    """
    Result type for Wooting Analog SDK.

    :cvar ok: Everything is fine
    :cvar uninitialized: Item hasn't been initialized
    :cvar no_devices: No Devices are connected
    :cvar device_disconnected: Device has been disconnected
    :cvar failure: Generic Failure
    :cvar invalid_argument: A given parameter was invalid
    :cvar no_plugins: No Plugins were found
    :cvar function_not_found: The specified function was not found in the library
    :cvar no_mapping: No Keycode mapping to HID was found for the given Keycode
    :cvar not_available: Indicates that it isn't available on this platform
    """

    ok = 1
    uninitialized = -2000
    no_devices = uninitialized + 1
    device_disconnected = uninitialized + 2
    failure = uninitialized + 3
    invalid_argument = uninitialized + 4
    no_plugins = uninitialized + 5
    function_not_found = uninitialized + 6
    no_mapping = uninitialized + 7
    not_available = uninitialized + 8


@dataclass
class DeviceInfo(c.Structure):
    """
    The core DeviceInfo class which contains all the interesting information for a particular device.

    :cvar vendor_id: Device Vendor ID `vid`
    :cvar product_id: Device Product ID `pid`
    :cvar manufacturer_name: Device Manufacturer name
    :cvar device_name: Device name
    :cvar device_id: Unique device ID, which should be generated using `generate_device_id`
    """

    vendor_id: int
    product_id: int
    manufacturer_name: bytes
    device_name: bytes
    device_id: int
    _fields_ = [
        ('vendor_id', c.c_uint16),
        ('product_id', c.c_uint16),
        ('manufacturer_name', c.c_char_p),
        ('device_name', c.c_char_p),
        ('device_id', c.c_uint64)
    ]