# WooPy
> Easy to use Python wrapper for the Wooting SKDs.

WooPy is a Python module that wraps the Wooting SDKs for easy use with Python. 
This version works with the new updated SDK version that integrates with the Wootility Software v3.4 or newer. 
So currently only the analog features are included.

For a version using the old RGB and analog SDK use the previous [commit](https://gitlab.com/_brady/woopy/tree/f58cc49c273d8f4d04fa61ff60087519d41a9150).

## Installation

Currently only tested on Windows 10, but it should also work on other operating systems.
Requires Python 3.7 or newer.

```sh
git clone https://gitlab.com/_brady/woopy.git
```
Download the current Wootility and the latest [wooting_analog_wrapper](https://github.com/WootingKb/wooting-analog-sdk/releases) for your operating system.

For more information on the Wooting SDKs and keymaps refer to:
* Analog SDK: https://github.com/WootingKb/wooting-analog-sdk
* RGB SDK: https://github.com/WootingKb/wooting-rgb-sdk

## Usage example

To get started have a look at the [analog_example.py](analog_example.py) and 
the official [SDK Usage Page](https://github.com/WootingKb/wooting-analog-sdk/blob/master/SDK_USAGE.md).

Simple example:
```python
from woopy.sdk import Analog

kbd = Analog('path_to/wooting_analog_wrapper.dll')
kbd.initialize()
print(kbd.read_analog(42))  # 42 is the keycode for Backspace
kbd.uninitialize()
```

It also supports usage as a context manager and will initialize and uninitialize automatically.
 ```python
from woopy.sdk import Analog

with Analog('path_to/wooting_analog_wrapper.dll') as kbd:
    print(kbd.read_analog(42))
```

## Release History
* 1.0 
    * Major rework of the project to work with the new Wooting Analog SDK
    * Use Enums and ReturnTypes that provide the full information as the C-API with an easy Python interface.
    * Provide the full documentation for the Wooting Analog SDK as Docstrings within Python.
* 0.2
    * Add method to add a Python function as a disconnect callback.
* 0.1
    * First version (old Wooting SDK)

## Meta

[Brady](https://gitlab.com/_brady/) – Contact me on the Wooting [Discord server](https://discordapp.com/invite/rNghtgw)

Distributed under the MIT license. See [LICENSE](LICENSE) for more information.